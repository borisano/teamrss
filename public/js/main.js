$(document).ready(function(){
    var $cache = {};
    initCache();
    initDOM();
    initEvents();

    function initCache() {
        $cache = {
            menuSources : $(".menu_sources"),
            atriclesRoot : $('#tabs').find('.articles_root'),
            tabs : $('#tabs'),
            templates : $('.global_templates'),
            newSourceContainer : $('.source_adding_container')
        }
    }

    function initDOM() {
        util.getAllSources(function( data ) {
            for( var i=0, len = data.length; i < len; i++ ) {
                var menuItem = util.clone( $cache.menuSources, "li" );
                var menuItemLink = menuItem.find('a').first();
                menuItemLink.html(data[i].title);
                menuItem.val( data[i].id );
                menuItem.attr('data-id', data[i].id );
                menuItemLink.attr( 'href', '#tabs-' + data[i].id );
                $cache.menuSources.append( menuItem );
                var articlesContainer = util.clone( $cache.atriclesRoot, '.articles_item' );
                articlesContainer.attr( 'id', 'tabs-'+data[i].id );
                $cache.atriclesRoot.append( articlesContainer );
            }
            $cache.tabs.tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
            $cache.tabs.find('li').removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
            showAllArticles();
            showFavourites();
            showSitesArticles();
        });
    }

    function initEvents() {
        $cache.menuSources.on('click', '.update_articles', function() {
            var sourceId = $(this).parent('.source_item').data('id');
            if(sourceId == 'all') {
                showAllArticles();
            } else if (sourceId == 'favourite') {
                showFavourites();
            } else {
                showArticlesBySite( sourceId );
            }
        })
        .on('click', '.delete_source', function() {
            var sourceId = $(this).parent('.source_item').data('id');
            util.removeSource( sourceId, function (response ) {
                window.location = window.location;
            });
        });

        $cache.atriclesRoot.on('click', '.favourite_toggle', function() {
            var article = $(this).parent('.rendering_article');
            var articleId = $(this).data('id');
            if ( article.hasClass('favourite_article') ) {
                $(this).parent('.rendering_article').removeClass('favourite_article');
                util.removeFromFavourite( articleId );
            } else {
                $(this).parent('.rendering_article').addClass('favourite_article');
                util.addToFavourite( articleId );
            }
        });
        
        $cache.atriclesRoot.on('click', '.read_toggle', function() {
            var article = $(this).parent('.rendering_article');
            var articleId = $(this).data('id');
            if ( article.hasClass('read_article') ) {
                return;
            } else {
                $(this).parent('.rendering_article').addClass('read_article');
                util.markAsRead( articleId );
                article.remove();
            }
        });

        $cache.newSourceContainer.on( 'click', '.add_source_button', function () {
            var sourceURL = $cache.newSourceContainer.find('.add_source').val();
            if ( !sourceURL ) {
                return false;
            }
            util.addNewSource( sourceURL, function ( response ) {
                console.log(response.success);
                if( response.success == false ) {
                    alert('Source is ivalide');
                    return false;
                }
                window.location.href = window.location.href;
            });
        });

        $cache.atriclesRoot.on( 'click', '.mark_read', function () {
            var articleId = $(this).val();
            var article = $(this).parent('.rendering_article');
            util.markAsRead( articleId, function( response ) {
                article.remove();
            })
        })
        .on( 'click', '.add_to_favourite', function () {
            var articleId = $(this).val();
            util.addToFavourite( articleId, function( response ) {

            })
        })
        .on( 'click', '.remove_from_favourite', function () {
            var articleId = $(this).val();
            util.removeFromFavourite( articleId, function( response ) {

            })
        });

    }

    function showSitesArticles() {
        $cache.menuSources.find('li').each( function() {
            var sourceId = $(this).data('id');
            if(sourceId != 'all' && sourceId != 'favourite') {
                showArticlesBySite( sourceId );
            }
        });
    }

    function updateArticles( articlesContainer, articles ) {
        for( var i=0, len = articles.length; i < len; i++ ) {
            var article = util.clone( $cache.templates, '.rendering_article' );
            article.find('.title').find('a').attr('href', articles[i].link);
            article.find('a').html(articles[i].title);
            article.find('.mark_read').val( articles[i].id );
            article.find('.favourite_toggle').attr('data-id', articles[i].id);
            article.find('.read_toggle').attr('data-id', articles[i].id);
            article.find('.add_to_favourite').val( articles[i].id );
            article.find('.remove_from_favourite').val( articles[i].id );
            if(articles[i].is_faved != '0') {
                article.addClass("favourite_article");
            }
            article.find('.description').html( unescape( Encoder.htmlDecode(articles[i].description) ) );
            article.find('.description').find('img').prependTo(article.find('.description'));
            articlesContainer.append(article);
        }
    }

    function showArticlesBySite ( sourceId ) {
        var articlesContainer = $('#tabs-' + sourceId);
        articlesContainer.empty();
        util.getArticlesBySource(sourceId, false, function( articles ) {
            updateArticles( articlesContainer, articles );
        });
    }

    function showAllArticles() {
        var articlesContainer = $('#tabs-all');
        articlesContainer.empty();
        util.getAllArcticles(false, function ( articles ) {
            updateArticles( articlesContainer, articles );
        })
    }

    function showFavourites() {
        var articlesContainer = $('#tabs-fav');
        articlesContainer.empty();
        util.getFavArcticles(null, function ( articles ) {
            updateArticles( articlesContainer, articles );
        })
    }

});

var util = {
    getJSON : function( url, data, callback ) {
        $.ajax({
            url: url,
            type : 'POST',
            context : this,
            data: data,
            success: function( response ) {
                if(typeof callback === 'function') {
                    callback( response );
                }
            }
        });
    },

    clone : function( container, selector ) {
        return container.find( selector + '.template' ).first().clone().removeClass('template');
    },

    getAllSources : function( callback ) {
        util.getJSON('rss/getallsources', {}, function( response ) {
            if( typeof callback === 'function' && response.success ) {
                callback( response.message );
            }
        });
    },

    getArticlesBySource : function( sourceId, newArticlesOnly, callback ) {
        util.getJSON('rss/getsourcearticles', {
                'source_id' : sourceId,
                'new_articles_only' : 1
            }, function( response ) {
            if( typeof callback === 'function' && response.success ) {
                callback( response.articles );
            }
        });
    },

    getAllArcticles : function( newArticlesOnly, callback ) {
        util.getJSON('rss/getallsourcesarticles', {
                'new_articles_only' : 1
            }, function( response ) {
            if( typeof callback === 'function' && response.success ) {
                callback( response.articles );
            }
        });
    },

    getFavArcticles : function( sourceId, callback ) {
        util.getJSON('rss/getsavedarticles', {
                'source_id' : sourceId
            }, function( response ) {
            if( typeof callback === 'function' && response.success ) {
                callback( response.articles );
            }
        });
    },

    addNewSource : function( sourceURL, callback ) {
        util.getJSON('rss/addsource', {
                'source' : sourceURL
            }, function( response ) {
            if( typeof callback === 'function' ) {
                callback( response );
            }
        });
    },

    removeSource : function( sourceURL, callback ) {
        util.getJSON('rss/removesource', {
                'source_id' : sourceURL
            }, function( response ) {
            if( typeof callback === 'function' ) {
                callback( response );
            }
        });
    },

    markAsRead : function( articleId, callback ) {
        util.getJSON('rss/markarticleasreeded', {
                'article_id' : articleId
            }, function( response ) {
            if( typeof callback === 'function' ) {
                callback( response );
            }
        });
    },

    addToFavourite : function( articleId, callback ) {
        util.getJSON('rss/savearticle', {
                'article_id' : articleId
            }, function( response ) {
            if( typeof callback === 'function' ) {
                callback( response );
            }
        });
    },

    removeFromFavourite : function( articleId, callback ) {
        util.getJSON('rss/unsavearticle', {
                'article_id' : articleId
            }, function( response ) {
            if( typeof callback === 'function' ) {
                callback( response );
            }
        });
    }
};


