<?php

class RssController extends Zend_Controller_Action
{

    public function init() {

    }

    public function preDispatch() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

    public function indexAction() {

    }

    /**
     * @param string source - source to be added in DB
     * @param string name   - name of source
     * @param string iconUrl - optional icon
     *
        $.ajax({
           url: 'rss/addsource',
           type : 'POST',
           context : this,
           data: {
               'source' : 'http://habrahabr.ru/rss',
               'name'  : 'Habr',
               'iconUrl' : 'http://habrahabr.ru/rss'
           },
           success: function( response ) {
               console.log(response);
           }
       });
     */
    public function addsourceAction() {

        header( 'Content-type: application/json' );
        try {
            $sourceUrl = htmlspecialchars( $this->getRequest()->getParam('source') );
            $sourceName = htmlspecialchars( $this->getRequest()->getParam('name') );
            $iconUrl = htmlspecialchars( $this->getRequest()->getParam('icon_url') );
            if( ! filter_var($sourceUrl, FILTER_VALIDATE_URL) ) {
                throw new Exception( "Source does not contain correct URL" );
            }

            $sourceModel = new Application_Model_DbTable_Source();
            $source = $sourceModel->createRow();
            $source->link = $sourceUrl;
            $source->title = $sourceName;
            $source->save();
            
            $response['success'] = true;
            $response['message'] = $sourceUrl;
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }

    /**
     * @param int $source_id
     *
     *
        $.ajax({
            url: 'rss/removesource',
            type : 'POST',
            context : this,
            data: {
                'source_id' : 1
            },
            success: function( response ) {
                console.log(response);
            }
        });
     */
    public function removesourceAction() {

        header( 'Content-type: application/json' );
        try {
            $sourceID = (int) $this->getRequest()->getParam('source_id');

            if( $sourceID < 1 ) {
                throw new Exception("Source ID is incorrect" );
            }

            $sourceModel = new Application_Model_DbTable_Source();
            $source = $sourceModel->find( $sourceID )->current();
            if( ! $source ) {
                throw new Exception("Source does not exists" );
            }

            // before removing source, we need to remove all related articles
            $relatedArticles = $source->findDependentRowset( 'Application_Model_DbTable_Article' );
            if( count( $relatedArticles ) ) {
                foreach ( $relatedArticles as $article ) {
                    $article->delete();
                } unset($article);
            }

            $source->delete();

            $response['success'] = true;
            $response['message'] = 'Source successfully removed';
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }

    public function getnumofunreadinsourceAction() {
        header( 'Content-type: application/json' );
        try {
            $sourceID = (int) $this->getRequest()->getParam('source_id');

            $sourceArticles = Application_Model_DbTable_Article::getSourceArticles( $sourceID );

            $response['success'] = true;
            $response['message'] = 'Total number is ' . count($sourceArticles);
            $response['numOfArticles'] = count($sourceArticles);
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }

    public function getallsourcesAction() {
        header( 'Content-type: application/json' );
        try {
            $sourcesModel = new Application_Model_DbTable_Source();

            $sources = $sourcesModel->fetchAll();

            $response['success'] = true;
            $response['message'] = $sources->toArray();
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }


    public function getallsourcesarticlesAction() {
        header( 'Content-type: application/json' );
        try {
            $newArticlesOnly = $this->getRequest()->getParam('new_articles_only') ? true : false;

            $sourceArticles = Application_Model_DbTable_Article::getAllArticles( $newArticlesOnly );

            $response['success'] = true;
            $response['message'] = 'Articles recieved. Total number is ' . count($sourceArticles);
            $response['articles'] = $sourceArticles->toArray();
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }
    /**
     * @param int source_id
     * @param 0/1 new_articles_only optional default TRUE
     *
        $.ajax({
           url: 'rss/getsourcearticles',
           type : 'POST',
           context : this,
           data: {
               'source_id' : 2,
               'new_articles_only' : 1
           },
           success: function( response ) {
               console.log(response);
           }
       });
     */
    public function getsourcearticlesAction() {

        header( 'Content-type: application/json' );
        try {
            $sourceID = (int) $this->getRequest()->getParam('source_id');
            $newArticlesOnly = $this->getRequest()->getParam('new_articles_only') ? true : false;

            $sourceArticles = Application_Model_DbTable_Article::getSourceArticles( $sourceID, $newArticlesOnly );

            $response['success'] = true;
            $response['message'] = 'Articles recieved. Total number is ' . count($sourceArticles);
            $response['numOfArticles'] = count($sourceArticles);
            $response['articles'] = $sourceArticles->toArray();
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }

    /**
     *@param int source_id - optional - get faved entries from specified source only
     *
        $.ajax({
           url: 'rss/getsavedarticles',
           type : 'POST',
           context : this,
           data: {
               'source_id' : 0
           },
           success: function( response ) {
               console.log(response);
           }
        });
     */
    public function getsavedarticlesAction() {

        header( 'Content-type: application/json' );
        try {
            $sourceID = (int) $this->getRequest()->getParam('source_id');

            $favedArticles = Application_Model_DbTable_Article::getSavedArticles( $sourceID );

            $response['success'] = true;
            $response['message'] = 'Articles recieved. Total number is ' . count( $favedArticles );
            $response['articles'] = $favedArticles->toArray();
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }

    /**
     * @param int article_id
     *
        $.ajax({
           url: 'rss/markarticleasreeded',
           type : 'POST',
           context : this,
           data: {
               'article_id' : 5
           },
           success: function( response ) {
               console.log(response);
           }
        });
     */
    public function markarticleasreededAction() {

        header( 'Content-type: application/json' );
        try {
            $articleID = (int) $this->getRequest()->getParam('article_id');

            $markingResult = Application_Model_DbTable_Article::markArticleAsReeded( $articleID );

            if( ! $markingResult ) {
                throw new Exception( "Error while marking article as readed" );
            }

            $response['success'] = true;
            $response['message'] = "Article succesfully marked as readed";
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }

    /**
     * @param int article_id
     *
        $.ajax({
           url: 'rss/savearticle',
           type : 'POST',
           context : this,
           data: {
              'article_id' : 5
           },
           success: function( response ) {
               console.log(response);
           }
        });
     */
    public function savearticleAction() {

        header( 'Content-type: application/json' );
        try {
            $articleID = (int) $this->getRequest()->getParam('article_id');

            $savingResult = Application_Model_DbTable_Article::saveArticle( $articleID );

            if( ! $savingResult ) {
                throw new Exception( "Error while article saving" );
            }

            $response['success'] = true;
            $response['message'] = "Article successfully saved";
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }

    /**
     * @param int article_id
     *
        $.ajax({
           url: 'rss/unsavearticle',
           type : 'POST',rss/removesource
           context : this,
           data: {
              'article_id' : 5
           },
           success: function( response ) {
               console.log(response);
           }
        });
     */
    public function unsavearticleAction() {

        header( 'Content-type: application/json' );
        try {
            $articleID = (int) $this->getRequest()->getParam('article_id');

            $savingResult = Application_Model_DbTable_Article::unsaveArticle( $articleID );

            if( ! $savingResult ) {
                throw new Exception( "Error while unsaving article" );
            }

            $response['success'] = true;
            $response['message'] = "Article successfully unsaved";
        }
        catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();

        }

        echo Zend_Json::encode($response);
    }


}

