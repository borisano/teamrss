<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function _initConfig() {
        $config = new Zend_Config_Ini(APPLICATION_PATH."/configs/application.ini","production");
        Zend_Registry::set( "config", $config );
        return $config;
    }

        public function _initDatabase() {
        $db = Zend_Db::factory( new Zend_Config_Ini( "../application/configs/db.ini" ));
        $db->query("SET NAMES UTF8");

        Zend_Db_Table::setDefaultAdapter( $db );

        Zend_Registry::set( "database", $db);
    }

}

