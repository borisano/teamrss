<?php

class Application_Model_DbTable_Article extends Zend_Db_Table_Abstract
{
    protected $_name = 'article';

    protected $_primary = 'id';

     protected $_referenceMap = array(
        'Albums' => array(
            'columns'       => 'source_id',
            'refTableClass' => 'Application_Model_DbTable_Source',
            'refColumns'    => 'id'
        ),
    );
     public static function getSavedArticles( $sourceID = null ) {
        $articleModel = new self();

        $select = $articleModel->select()
            ->where("is_faved = ?", 1);
        if( $sourceID > 0 ) {
            $select->where( 'source_id = ?', $sourceID ); // add new condition
        }

        $favedArticles = $articleModel->fetchAll( $select );

        return $favedArticles;
     }

     public static function getAllArticles( $newArticlesOnly = false  ) {
         Application_Model_DbTable_Source::updateAllSources();
        $articleModel = new self();

        $select = $articleModel->select()
                                ->order( 'pub_date DESC' );

        if( $newArticlesOnly ) {
            $select->where( 'is_readed = ?', "0" ); // add new condition
        }

        $articlesOfSource = $articleModel->fetchAll( $select );

        return $articlesOfSource;
     }


     public static function getSourceArticles( $sourceID, $newArticlesOnly = false ) {
        Application_Model_DbTable_Source::updateSource( $sourceID );
        $articleModel = new self();

        $select = $articleModel->select()
            ->where("source_id = ?", $sourceID)
            ->order( 'pub_date DESC' );
            //TODO - сортировать по дате
        if( $newArticlesOnly ) {
            $select->where( 'is_readed = ?', "0" ); // add new condition
        }

        $articlesOfSource = $articleModel->fetchAll( $select );

        return $articlesOfSource;
     }

     public static function markArticleAsReeded( $articleID ) {
          $articleModel = new self();

          $article = $articleModel->find( $articleID )->current();
          if( ! $article ) {
              throw new Exception("There's no article with ID=" . $articleID . " in DB");
          }

          $article->is_readed = 1;

          return (bool) $article->save(); // save() method returns article id on correct saving
     }

     // 'saving' means user wants to return to this article later
    public static function saveArticle( $articleID ) {
          $articleModel = new self();

          $article = $articleModel->find( $articleID )->current();
          if( ! $article ) {
              throw new Exception("There's no article with ID=" . $articleID . " in DB");
          }

          $article->is_faved = 1;

          return (bool) $article->save(); // save() method returns article id on correct saving
     }

          // 'saving' means user wants to return to this article later
    public static function unsaveArticle( $articleID ) {
          $articleModel = new self();

          $article = $articleModel->find( $articleID )->current();
          if( ! $article ) {
              throw new Exception("There's no article with ID=" . $articleID . " in DB");
          }

          $article->is_faved = 0;

          return (bool) $article->save(); // save() method returns article id on correct saving
     }

     /**
      * Add article to DB in case it's not there already
      *
      * @param int $sourceID
      * @param string $link
      * @param string $title
      * @param string $description
      *
      * @return mixed
      *             Row - if new item is created, row object of this array
      *             false - if such item already exists
      */
     public static function addArticle( $sourceID, $link, $title, $description, $pubDate ) {
         $articleModel = new self();

         $select = $articleModel->select()
                                ->from( $articleModel->_name )
                                ->where( "source_id = ?", $sourceID )
                                ->where( "link = ?", $link );

         $article  = $articleModel->fetchRow( $select );
         if( $article ) {
             return false;
         }

        $article = $articleModel->createRow(array(
                      'source_id'  => (int) $sourceID,
                      'link'       => htmlspecialchars( $link ),
                      'title'      => htmlspecialchars( $title ),
                      'description'=> htmlspecialchars( $description ),
                      'pub_date'   => $pubDate,
                      'is_readed'  => 0,
                      'is_faved'   => 0
                   ));
        $article->save();

        return $article;
     }

}