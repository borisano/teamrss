<?php

class Application_Model_DbTable_Source extends Zend_Db_Table_Abstract
{
    protected $_name = 'source';

    protected $_primary = 'id';

    protected $_dependentTables = array(
        'Application_Model_DbTable_Article'
     );


     /**
      * Check source for updates.
      * @param int $sourceID
      *
      * @return bool - updating result
      *
      * Function DOES NOT return any articles information. It only updates DB.
      * To retrieve data, use getSourceArticles() method instead
      */
     public static function updateSource( $sourceID, $sourceOnly = false ) {
        try{
            $sourceModel = new self();
            $source = $sourceModel->find( $sourceID )->current();
            if( ! $source ) {
                throw new Exception( "Source with id $sourceID does not exists" );
            }

            $rssPage = file_get_contents( $source->link );
            $xml = new SimpleXmlElement($rssPage);

            $source->link = (string) $xml->channel->link;
            $source->title= (string) $xml->channel->title;
            $source->description = (string) $xml->channel->description;

            $source->save();
            
            // update source items
            foreach($xml->channel->item as $entry) {

                $link = (string) $entry->link;
                $title = (string) $entry->title;
                $desc = (string) $entry->description;
                //Sun, 19 May 2002 15:21:36 GMT
              //  return date('Y-m-d H:i:s', strtotime($str));
                $pD = ( $entry->pubDate . '');
                $pubDate = date('Y-m-d H:i:s', strtotime( $pD ));

                Application_Model_DbTable_Article::addArticle($sourceID, $link, $title, $desc, $pubDate );
            }
            return true;
        }
        catch( Exception $e ) {
            return false;
        }


     }

     public static function updateAllSources() {
         $sourcesModel = new self();

         $sources = $sourcesModel->fetchAll();
         foreach ($sources as $source) {
             Application_Model_DbTable_Source::updateSource( $source->id );
         }
     }


}